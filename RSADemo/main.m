//
//  main.m
//  RSADemo
//
//  Created by 方鹏俊 on 16/11/15.
//  Copyright © 2016年 方鹏俊. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
